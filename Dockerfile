FROM phusion/baseimage
MAINTAINER Hansel Ke

# Use the phusion/baseimage init system
CMD ["/sbin/my_init"]

# Install Route53
RUN apt-get update && apt-get install -y git python python-pip && apt-get clean
RUN git clone https://hanselke@bitbucket.org/openbiz/route53.git route53
RUN rm -rf /opt/route53
RUN mv -f route53/ /opt/
RUN pip install -r /opt/route53/requirements.txt

